import EStyleSheet from 'react-native-extended-stylesheet';

export const form = EStyleSheet.create({
   StyleForm : {
       padding: 20
   },
    item : {
        borderRadius : 5,
        marginBottom:10,
        paddingRight:10,
        paddingLeft: 10,
        backgroundColor:'white'
    },
    input : {
       fontFamily: '$IS',
        fontSize:14,
        textAlign: 'right'
        
    },
    submitButton : {
       borderRadius: 5,
      
        backgroundColor: 'white',
        justifyContent : 'center',
        alignItems:'center',
        
        
    },
    submitText : {
       fontSize : 18,
       
        color:'black'
    },
    error : {
       fontFamily : '$IS',
        fontSize:12 ,
        color : 'white',
        marginBottom: 10
    }
});

export const index = EStyleSheet.create({
    splashContainer : {
        flex: 1 ,
        justifyContent: 'center' ,
        alignItems: 'center' ,
        backgroundColor : '#B22D30'
    },
    splashText : {
        color : 'white',
        fontSize : 18,
        fontFamily : '$IS'
    }
});

export const drawer = EStyleSheet.create({
    container :{
        flex: 1
    },

    imageView : {
        flex:2,
        justifyContent:'center',
        alignItems:'center'

    },

    itemView : {
        flex:5,
        
    },

     imageHeader : {
        flex:1,
         
     },
     item : {
         justifyContent: 'flex-end' ,
         padding: 10,
         borderTopWidth:0,
         borderColor : 'white'
     },
     itemCenter : {
         flexDirection : 'column',
        justifyContent: 'center' ,
        padding: 10,
        borderTopWidth:0,
        borderColor : 'white'
    },
     itemTitle : {
        fontFamily : '$IS'
     },
     itemIcon : {
        marginLeft: 10
     }
 });
export default styles = {
    index
};