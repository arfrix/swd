import React from 'react';
import { Container , Header , Right , Button,ScrollableTab , Content,Tabs,Tab , Text ,ListItem, CheckBox ,Body , Item ,Input   } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image ,View,Dimensions,Alert,AsyncStorage,ScrollView , KeyboardAvoidingView } from 'react-native';
import OrderCard from './ordersCard';
import FoodCardtruct from './foodCardStruct';


const urls =  require("../libs/urls")
const date = require ("../libs/date")
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;





export default class wallet extends React.Component{

    constructor(props){
        super(props);
        if (this.props.toPay == null || this.props.toPay == undefined || this.props.toPay == ""){
            this.props.toPay = 0
        }
        //alert(this.props.toPay)
        this.state={
            showTextBox : true,
            credit:"-",
            amount:this.props.toPay
        }
        
        
    }
    componentDidMount(){
        this.getCredit()
    }
    async getCredit(){
        try{
            
            const phoneNumber = await AsyncStorage.getItem("phoneNumber");
            try{
                
                const token = await AsyncStorage.getItem("token");
                fetch(urls.SERVER+urls.studentGetCredit,{
                    method:"POST",
                    headers:{
                        "Content-Type":"application/json"
                    },
                    body:JSON.stringify({
                        phoneNumber:phoneNumber,
                        token:token,
                        
                    })
                })
                .then(resp=>{
                    return resp.json()
                })
                .then(json=>{
                    //alert(json);
                    this.setState(prev=>{
                      prev.credit = json
                      return prev
                    })
                }).catch(e=>{
                    alert("Error::"+e)
                })
            }catch(e){
                alert("Error:::"+e)
            }
        }catch(e){
            alert("Error::::"+e)
        }
    }

    async submit(){
        try{
            //alert(this.props.toPay)
            //alert(this.state.amount)
            const phoneNumber = await AsyncStorage.getItem("phoneNumber");
            try{
                
                const token = await AsyncStorage.getItem("token");
                fetch(urls.SERVER+urls.studentAddCredit,{
                    method:"POST",
                    headers:{
                        "Content-Type":"application/json"
                    },
                    body:JSON.stringify({
                        phoneNumber:phoneNumber,
                        token:token,
                        amount:this.state.amount
                        
                    })
                })
                .then(resp=>{
                    alert(JSON.stringify(resp))
                    Actions.pardakht({refID:resp})
                })
                .then(json=>{
                    alert(JSON.stringify(json));
                    
                }).catch(e=>{
                    alert("Error::"+e)
                })
            }catch(e){
                alert("Error:::"+e)
            }
        }catch(e){
            alert("Error::::"+e)
        }
    }

    render(){

        return(





            <View style={{flex:1,flexDirection:'column',backgroundColor:'#F8F8F8',justifyContent:'space-between'}}>

                <View style={{height:height*0.5}}>

                    <View style={{flex:2,justifyContent:'space-between' , flexDirection:'row',paddingTop:25}}>
                        <Text style={{marginLeft:width*0.09 , fontSize:width*0.07}} > {this.state.credit} تومان </Text>
                        <Text style={{marginRight:width*0.07 , fontSize:width*0.06}} > موجودی شما  </Text>

                    </View>

                

                



                    <View style={{flex:3 , flexDirection:'column'}}>
                        <View style={{flex:1,justifyContent:'center', flexDirection:'column',alignItems:'center'}}>
                            
                        
                    
                            <View style={{flex:1,height:height*0.1,justifyContent:'center'}} >       
                                <Text style={{marginRight:width*0.01 ,paddingTop:10 , fontSize:width*0.02 ,fontSize:width*0.06 , color:'red' }}
                                onPress={()=>
                                    this.setState(prev => {return {showTextBox : !prev.showTextBox}; })} >   شارژ به میزان دلخواه    </Text>
                            </View>
                    
                        </View>

                                
                            <View style={{flex:1,marginBottom:10 , marginTop: 10}} >

                                <Item style={{borderBottomWidth:2 ,justifyContent:'flex-end',marginRight:25,marginLeft:40}}>
                                    
                                    <Input   style={{width:width*0.5}} defaultValue={this.props.toPay} onChangeText={
                                        (text)=>{
                                            
                                            this.setState(prev=>{
                                                prev.amount = text
                                                return prev
                                            })
                                        }
                                    }/>
                                    <Text> مبلغ مورد نظر به ریال  :</Text>
                                </Item>

                            </View>    
                    </View>

                </View>   

                     <View>
                        <Button full style={{backgroundColor:'#B22D30'}} onPress={
                            ()=>{
                                this.submit()
                            }
                        }> 
                            <Text> افزایش اعتبار </Text>
                        </Button>    

                    </View>   
               

            </View>





            
        )

    }

}