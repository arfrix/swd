import React from 'react';
import { Container, Header, Content, Card, CardItem, Thumbnail,
   Text, Button, Icon, Left, Body, Right,Tabs,Tab,ScrollableTab,Drawer,List,ListItem } from 'native-base';
import {  View, Image,Dimensions,ScrollView,StyleSheet,AsyncStorage} from 'react-native';
import { form } from './../assets/styles';
import { Actions } from 'react-native-router-flux';
import StarRating from 'react-native-star-rating';
import FoodCard from './foodCard';
import CoffeeShopCard from './coffeeShopCard';
import BreakfastCard from './breakfastCard';
import FoodCardtruct from './foodCardStruct';
import SideBar from './sideBar';


const urls = require("../libs/urls")
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default class FoodsList extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        listfoodFromCard:[],
        credit:"-",
        Irani:[],
        Cafe:[],
        FastFood:[],
        Italian:[],
        BreakFast:[],
        Games:[]
      };    
      this.games = [{ "firstTeam" : "Iran", "secondTeam" : "America", "gameID" : "12", "date" : "2018", "live" : false, "result" : "2-1", "_class" : "io.backdoor.sharifplus_backend_kotlin.bet.Game" }]
  }
  componentDidMount(){
    /*setInterval(()=>{
      this.getCredit();
    },1000)*/
    this.getCredit()
    //this.getGames()
    
    //this.getFoods()

  }

  getTeamString(game){
    alert(`${game.firstTeam} - ${g.secondTeam}`)
    return `${game.firstTeam} - ${g.secondTeam}`
  }
  async getGames(){
    try{
      const phoneNumber = await AsyncStorage.getItem("phoneNumber")
      try{
        const token = await AsyncStorage.getItem("token")
        fetch(urls.SERVER + urls.getGames,{
          method:"POST",
          headers:{
            "Content-Type":"application/json"
          },
          body:JSON.stringify({
            phoneNumber:phoneNumber,
            token:token
          })
        })
        .then(resp=>resp.json())
        .then(respJson=>{
          //alert(JSON.stringify(respJson))
          this.setState(prev=>{
            prev.Games = respJson;
            
            return prev;
          });
        })
      }catch(e){
        alert(e)
      }
    }catch(e){
      alert(e)
    }
  }
  async getCredit(){
    try{
        const phoneNumber = await AsyncStorage.getItem("phoneNumber");
        try{
            
            const token = await AsyncStorage.getItem("token");
            fetch(urls.SERVER+urls.studentGetCredit,{
                method:"POST",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    phoneNumber:phoneNumber,
                    token:token
                })
            })
            .then(resp=>{
                return resp.json()
            })
            .then(json=>{
                //alert(json);
                this.setState(prev=>{
                  prev.credit = json
                  return prev
                })
            }).catch(e=>{
                alert("Error::"+e)
            })
        }catch(e){
            alert("Error:::"+e)
        }
    }catch(e){
        alert("Error::::"+e)
    }
}

  getFoods(){
    //alert("@getFood")
    fetch(urls.SERVER+urls.foodGetAll,{
      method:"GET",
      headers:{
        "Content-Type":"application/json"
      }
    })
    .then(reposonse=>reposonse.json())
    .then(json=>{
      //alert("::::"+JSON.stringify(json))
      json.map(f=>{
        this.setState(prev=>{
          prev[f.type].push(f)
          return prev
        })
      })
      
    })
  }
  
  myCallback = (food) => {
    //this.setState({ listfoodFromCard : [...this.state.listfoodFromCard , dataFromChild] });
  //  console.warn(dataFromChild)
    this.setState(prev=>{
      prev.listfoodFromCard.push(food)
      //alert(JSON.stringify(food))
      return prev
    })
   // console.warn(this.state.listfoodFromCard)
  }

  

  closeDrawer = () => {
    this.drawer._root.close()
  };
  openDrawer = () => {
    this.drawer._root.open()
  };

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }



    render() {


        return (
     
       /*   <View style={{flex: 1,
            flexDirection: 'column',
            justifyContent: 'center'}}>
                
                  <View style={{backgroundColor : '#B22D30',flex:0.1,  flexDirection: 'row',   justifyContent: 'center',  }}>
                  <Icon name="more" onPress={() => Actions.drawerOpen()} style={{position:'absolute',right:10}}/>
                  <Image 
                style={{ height: height*0.25}}
                source={require('./../assets/images/logo.png')}/>
                 </View>

            <View style={{}}>
           <Tabs style={{backgroundColor:'white'}} tabBarUnderlineStyle={{borderBottomWidth:2,borderBottomColor:'#B22D30'}} renderTabBar={()=> <ScrollableTab />}>
              <Tab heading="کافه" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}} >
              <View style={{flexWrap:'wrap'}}>
              
              <CoffeeShopCard/>
              <CoffeeShopCard/>
              <CoffeeShopCard/>
              <CoffeeShopCard/>
              <CoffeeShopCard/>
              <CoffeeShopCard/>
              <CoffeeShopCard/>
              
              </View>
              </Tab>
              <Tab heading="ناهار" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{flexWrap:'wrap'}}> 
                  <FoodCard/>
                  <FoodCard/>
                  <FoodCard/>
                  <FoodCard/>
                  </View> 
              </Tab>
              <Tab heading="صبحانه" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{flexWrap:'wrap'}}> 
              <BreakfastCard/>
              <BreakfastCard/>
              <BreakfastCard/>
              </View>
              </Tab>
  
            </Tabs>
          
        </View>


            
              
          <View>

          

          </View>
 




      </View>

*/





          <View style={styles.container}>
          <View style={[styles.header]}>
          <Icon name="menu" onPress={() => Actions.drawerOpen({credit:this.state.credit})} style={{position:'absolute',top:8,right:15}}/>
                  <Image 
                style={{ }}
                source={require('./../assets/images/logo.png')}/>
          
          </View>
          <ScrollView>
            <View style={[styles.content]}>
            <Tabs  style={{backgroundColor:'white'}} tabBarUnderlineStyle={{borderBottomWidth:2,borderBottomColor:'#B22D30'}} renderTabBar={()=> <ScrollableTab />}>


              <Tab heading="پیش بینی" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{flexWrap:'wrap'}}> 
             
              
              
                <List>
                  
                   {
                     this.games.map(g=>{
                       //alert(JSON.stringify(g))
                          return (
                          <ListItem  onPress={()=>{
                            Actions.forcastResult()
                          }}>
                          <View  style={{flex:1 , flexDirection:'row',justifyContent:'center'}}>
                          <Body style={{flex:1 , flexDirection:'row',justifyContent:'center'}}>
                          <Thumbnail square size={80} source={{ uri: 'https://ssl.gstatic.com/onebox/media/sports/logos/z3JEQB3coEAGLCJBEUzQ2A_48x48.png' }} />
                          <View style={{  width:width*0.5, flexDirection:'column',justifyContent:'center'}}>
                            <View style={{flex:1 , flexDirection:'row',justifyContent:'center',marginBottom:10}}>
                              <Icon name="pizza"  style={{marginRight:10}}/>
                              <Icon name="play" style={{marginLeft:10}}/>
                            </View> 
                            <View style={{flex:1 , flexDirection:'column',justifyContent:'center',alignItems:'center'}}> 
                              <Text>{g.firstTeam + "-" + g.secondTeam}</Text>
                              <Text note>ساعت 17:30</Text>
                            </View>
                          </View>
                          <Thumbnail square size={80} source={{ uri: 'https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png' }} />  
                          </Body>
                          </View>
                          </ListItem>
                        )
                        })
                   }
                   
                    
                    
                    
                          
                        
                      
    
                 
                </List>
                
              </View>
              </Tab>

              <Tab heading="گیم سنتر" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{height:height*0.6,justifyContent:'center',alignItems:'center',zIndex:-10}}> 
                <Text style={{fontSize:width*0.08}}>بزودی !</Text>
              </View>
              </Tab>


              <Tab heading="کافه" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}} >
              <View style={{flexWrap:'wrap'}}>
              <Text style={{fontSize:width*0.08}}>بزودی !</Text>

       
              {
                //this.state.Cafe.map(f=>{
                //return <View><FoodCard obj={f} name={f.name} price={f.price} callbackFromParent={this.myCallback}/></View>
              //})
              }
             
              
              </View>
              </Tab>
              <Tab heading="فست فود" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{flexWrap:'wrap'}}> 
              <Text style={{fontSize:width*0.08}}>بزودی !</Text>
              {
                //this.state.FastFood.map(f=>{
                //return <View><FoodCard obj={f} name={f.name} price={f.price} callbackFromParent={this.myCallback}/></View>
              //})
            }
                  </View> 
              </Tab>
              <Tab heading="پاستا" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{flexWrap:'wrap'}}> 
              <Text style={{fontSize:width*0.08}}>بزودی !</Text>

              {
                //this.state.Italian.map(f=>{
                //return <View><FoodCard obj={f} name={f.name} price={f.price} callbackFromParent={this.myCallback}/></View>
              //})
            }
                  </View> 
              </Tab>
              <Tab heading="غذای ایرانی" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{flexWrap:'wrap'}}> 
              <Text style={{fontSize:width*0.08}}>بزودی !</Text>

                {
                  //this.state.Irani.map(f=>{
                 // return <View><FoodCard obj={f} name={f.name} price={f.price} callbackFromParent={this.myCallback}/></View>
                //})
              }

                  </View> 
              </Tab>
              <Tab heading="صبحانه" tabStyle={{backgroundColor:'white'}} textStyle={{color:'black'}} activeTabStyle={{backgroundColor:'white'}} activeTextStyle={{color:'#B22D30'}}>
              <View style={{flexWrap:'wrap'}}> 
              <Text style={{fontSize:width*0.08}}>بزودی !</Text>

              {
                //this.state.BreakFast.map(f=>{
                //return <View><FoodCard obj={f} name={f.name} price={f.price} callbackFromParent={this.myCallback}/></View>
              //})
            }
              </View>
              </Tab>

              

              
            </Tabs>
            </View>
          </ScrollView>
          <View style={[styles.footer]}>
          <Button  style={{width:width,justifyContent:'center',backgroundColor:'#B22D30'}} onPress={() => Actions.daySelections({list :this.state.listfoodFromCard})}>
            <Text>
              سبد خرید
            </Text>
            </Button>
          
          </View>
          </View>












          




        )
    }
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  header: {
    flex:1,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    backgroundColor: '#B22D30',
    zIndex: 10,
    flexDirection: 'row',
       justifyContent: 'center'
  },
  content: {
    flex:7,
    alignItems: 'center',
    marginTop: height*0.23,
    marginBottom: 40
  },
  footer: {
    flex:1,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#8BC34A',
   
  },
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#333',
    marginBottom: 10
  }
});