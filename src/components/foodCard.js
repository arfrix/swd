import React from 'react';
import { Container, Header, Content, Card, CardItem, Thumbnail,
   Text, Button, Icon, Left, Body, Right,Tabs,Tab,ScrollableTab,Drawer } from 'native-base';
import {  View, Image,Dimensions,TouchableOpacity } from 'react-native';
import { form } from './../assets/styles';
import { Actions } from 'react-native-router-flux';
import StarRating from 'react-native-star-rating';


var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;


export default class foodCard extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
          starCount: 3.5,
          isClicked : false,
          add:'اضافه کن'
          
        };
      }


      addToBasket = () => {
        //alert(JSON.stringify(this.props.obj))
        this.props.callbackFromParent(this.props.obj);
        this.setState(prev =>{
          return{isClicked :true , add:'اضافه شد !'}
        })
      }

    
      onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
      }

render(){
    return(

<Card style={{marginLeft:width*0.02,marginRight:width*0.02}}>
               


               <View key="main" style={{height:height*0.18,flexDirection:'row',paddingLeft:5}}>
 
 
                 <View key="3" style={{flex:2,flexDirection:'column'}}>
 
                   <View key="7" style={{flex:1,justifyContent:'center'}}>
                   <StarRating
                     starSize={20}
                     disabled={false}
                     maxStars={5}
                     rating={this.state.starCount}
                     selectedStar={(rating) => this.onStarRatingPress(rating)}
                    />
                   </View>
                   <View key="8" style={{flex:1.8}}>
                   </View>
                   <View key="9" style={{flex:0.3,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                         
                    {/* <Icon name='remove' style={{position:'absolute',left:5}}/>
                      <Text>
                        2
                      </Text> 
                     <Icon name='add'style={{position:'absolute',right:5}} /> */}
 
                   </View>
 
 
                 </View>  
            
                  
             
                
 
                 <View style={{ flex: 3 , flexDirection: 'column',marginRight:5}} key="2">
                   
                 
                 <View style={{flex:2,alignItems:'flex-end',marginTop:height*0.02}}>
                 <Text style={{paddingBottom:0}}>{this.props.name}</Text>
                   </View>
                   <View style={{flex:5,justifyContent:'center',alignItems:'flex-end'}}>
                   <Text style={{fontSize:width*0.06}}>{this.props.price} تومان</Text>
                   </View>
                   <View style={{flex:2,position:'absolute',right:width*0.45,top:height*0.09,alignItems:'flex-start',marginBottom:8}}  >
                       
                           <Text style={[{color:"red",borderWidth:1,borderRadius:5,paddingRight:6,paddingLeft:4,paddingTop:2},this.state.isClicked && {borderColor:'green',backgroundColor:'#26de81'}]} onPress={this.addToBasket}>
                            {this.state.add}
                           </Text>
                       
                   </View>
 
                       
 
                      
 
                       
                   
                   </View>
 
                   <View key="1" style={{flex:2 , height:height*0.17 }}>
                    <Image 
                      style={{width:width*0.27, height:height*0.17 , position:'absolute',right:0 }}
                      source={require('./../assets/images/burger.jpg')}/>
                   </View>
                
                
           
              </View> 
           </Card>








    )
}

}