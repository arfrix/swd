import React from 'react';

import { Container , Header  , Text , Left , Button , Right , Content , Form , Item , Icon , Input } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { form } from './../assets/styles';
import { Image ,View,Dimensions,AsyncStorage,ScrollView } from 'react-native';

const urls = require("../libs/urls")
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;



export default class verifyCode extends React.Component {
    
    async verify(){
        try{
            const phoneNumber = await AsyncStorage.getItem("phoneNumber")
                //alert(phoneNumber)
                fetch(urls.SERVER+urls.studentVerify,{
                    method:"POST",
                    headers:{
                        "Content-Type":"application/json"
                    },
                    body:JSON.stringify({
                      phoneNumber: phoneNumber,
                      code:this.state.code
                    })
                  })
                  .then((response)=>{
                      //alert(JSON.stringify(response))
                      return response.json()
                  })
                  .then(async (respJson)=>{
                      //alert(JSON.stringify(respJson))
                      if (respJson.token === null || respJson.phoneNumber === null){
                          alert("error")
                          return
                      }else{
                          try{
                              await AsyncStorage.setItem("phoneNumber",respJson.phoneNumber);
                              try{
                                  await AsyncStorage.setItem("token",respJson.token);
                                  Actions.FoodsList();
                              } catch(err){
                                  alert(err)
                              }
                          } catch(err){
                              alert(err)
                          }
                      }
                  })
                  .catch(err=>{
                    alert(err)
                  })
                }catch(e){
                    alert(e)
                }
        }   
        
    constructor(){
        super();
        this.state={
            code:""
        }
    }


  



    render() {
        return (
            <Container>
               
               

                <View style={{flex:1}}>

                     
                   <View style={{flex:3,flexDirection:'row',justifyContent:'center',alignItems:'center' , backgroundColor:'#B22D30'}}>

                    <Image 
                         style={{width: 200, height: 120}}
                         source={require('./../assets/images/logo.png')}/>
                     </View>



                     <View style={{flex:6.5,flexDirection:'column', justifyContent:'center', backgroundColor:'#B22D30'}}>
                    <Form style={form.StyleForm} >
                        <Item rounded style={form.item} error={this.state.userNameError != ''}>
                            <Input
                                onChangeText={(code)=>{
                                    this.setState(prev=>{
                                        prev.code = code
                                        return prev
                                    })
                                }}
                                placeholder='کد ارسال شده را وارد نمایید'
                                style={form.input}
                            />
                            <Icon active name='md-mail' />
                        </Item>
                        <Text style={form.error}>{this.state.userNameError}</Text>
                        
                        
                    </Form>
                    </View>


                   <View style={{flex:0.9 , backgroundColor:'#B22D30',justifyContent:'center'}}>
                    <Button full style={form.submitButton} onPress={() =>this.verify()}>
                            <Text style={form.submitText}>تایید</Text>
                        </Button>

                   </View>
                   
                   </View>
              
            </Container>


        )
    }
}