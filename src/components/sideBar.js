import React, { Component } from 'react';
import {Text, Item, Icon , Button} from 'native-base';
import {Image,Dimensions,View,AsyncStorage} from "react-native";

import {drawer} from './../assets/styles'
import { Actions } from 'react-native-router-flux';


var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
const urls = require("../libs/urls")
export default class Sidebar extends Component {
    constructor(props){
        super(props)
        this.state={
            credit:"-"
        }
        this.getCredit()
    }
    async getCredit(){
        try{
            const credit = AsyncStorage.getItem("credit")
            this.setState(prev=>{
                prev.credit = credit
                return prev
            })
        }catch(e){
            alert(e)
        }
    }
  render() {
    return (
          
            
        <View style={drawer.container}>
        <View style={{flex:1.8,justifyContent:'center',alignItems:'center',padding:height*0.05}}>
          <Image source={{ uri  : 'https://roocket.ir/public/image/2017/8/9/react-native.png'}} style={{width:height*(2/9),height:height*(2/9),borderRadius:height*(2/18)}}/>
          <Text style={{padding:height*0.01,fontSize:width*0.05}}> 95170345 </Text>
        </View>  
        <View style={drawer.itemView}>
            <Item style={{justifyContent:'space-around',borderTopWidth:0,borderColor : 'white' , alignItems:'center'}}>
            <View >
                <Button title="hey" bordered danger style={{borderRadius:25,height:height*0.06 ,marginLeft : 5}} onPress={()=>{
                    Actions.wallet({toPay:0})
                }}>
                    <Text >  افزایش اعتبار </Text>
                </Button>
             </View>
        
            <View style={{flex:2,flexDirection:'row',justifyContent: 'flex-end',padding: 10 ,}}>
                
                
                <Text style={drawer.itemTitle}>  {this.props.credit} تومان {this.props.credit}</Text>
                <Icon name="cash" style={drawer.itemIcon}/>
                </View>  
            </Item>
            <Item style={drawer.item} >
                <Text style={drawer.itemTitle} onPress={() => Actions.orders()}>لیست سفارشات</Text>
                <Icon name="list-box" style={drawer.itemIcon}/>
            </Item>

            <Item style={drawer.item} >
                <Text style={drawer.itemTitle} onPress={() => Actions.orders()}> اطلاعات کاربری</Text>
                <Icon name="person" style={drawer.itemIcon}/>
            </Item>

            <Item style={drawer.item} >
                <Text style={drawer.itemTitle} onPress={() => Actions.orders()}>تماس با ما</Text>
                <Icon name="call" style={drawer.itemIcon}/>
            </Item>

            <Item style={drawer.itemCenter} >
                <Text style={drawer.itemTitle} > لیگ پیش بینی جام جهانی </Text>
                <View style={{flexDirection:'row',paddingTop:10}}>
                <Text> رتبه </Text>
                <Text> 33 </Text>
                <Text style={{marginLeft:15}}> امتیاز </Text>
                <Text> 123 </Text>
                </View>
            </Item>

            <Item style={drawer.itemCenter} >
                <Text style={drawer.itemTitle} > باشگاه مشتریان </Text>
                <View style={{flexDirection:'row'}}>
                <Text> امتیاز </Text>
                <Text> 123 </Text>
                </View>
            </Item>
            
        </View>
  </View>



         
    );
  }
}

module.exports = Sidebar;